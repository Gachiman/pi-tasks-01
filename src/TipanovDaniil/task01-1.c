#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
int main()
{
	int rub=0, cop=0;
	printf("Print line (Ex.: 15.21): ");
	if (scanf("%d.%d", &rub, &cop) == 0)
	{
		printf("INPUT ERROR!\n");
		return 1;
	}
	if (cop >= 100)
	{
		rub += cop / 100;
		cop %= 100;
	}
	printf("%d rub. %d cop.\n", rub, cop);
	return 0;
}