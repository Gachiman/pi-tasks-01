#include <stdio.h>

int main()
{
    int i = 0,sum = 0,currentNumber=0;
    char str[256];
    printf("Enter a string:");
    fgets(str,256,stdin);
    while (str[i])
    {
        if ((str[i]>='0') && (str[i]<='9'))
            currentNumber = currentNumber*10 + (str[i] - '0');
        else
        {
            sum += currentNumber;
            currentNumber = 0;
        }
        i++;
    }
    printf("Sum of numbers in your string - %d", sum);
    return 0;
}
