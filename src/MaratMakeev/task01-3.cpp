#include <stdio.h>
int main()
{
	char inputLine[256];
	int num = 0, sum = 0, i = 0;
	printf("Enter string: ");
	fgets(inputLine, 256, stdin);
	while (inputLine[i])
	{
		if (inputLine[i] >= '0' && inputLine[i] <= '9')
			num = num * 10 + (inputLine[i] - '0');
		else
		{
			sum += num;
			num = 0;
		}
		i++;
	}
	printf("Result : %d\n", sum);
	return 0;
}