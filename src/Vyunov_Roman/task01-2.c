#define _CRT_SECURE_NO_WARNINGS
#define INCHES_IN_FEET (12)
#define CM_IN_INCH (2.54)

#include <stdio.h>

int main()
{
	unsigned int feet = 0, inches = 0;
	unsigned double cm = 0;
	
	printf("Enter growth as feet\'inches : ");
	scanf("%d'%d",&feet,&inches);
	cm = (feet*INCHES_IN_FEET + inches)*CM_IN_INCH;
	printf("%.2f centimeters\n", cm);
	
	return 0;
}