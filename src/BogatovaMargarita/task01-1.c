#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>

int main()
{
	int r = 0, k = 0; char a[2];
	printf("Enter the price in the format RR.KK ");
	scanf("%i %c %i", &r, &a[0], &k);
	while (r < 0 || k < 0 || a[0]!='.')
	{
		puts("Error! Enter correctly");
		scanf("%i %c %i", &r, &a[0], &k);
	}
	if (k>99)
		{
			r += k / 100; k = k % 100;
		}
	printf("%i rub, %i kop \n", r, k);
	system("pause");
	return(0);
}