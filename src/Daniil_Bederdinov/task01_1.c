#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>

int main()
{
    int rubles = 0;
    int kop = 0;

    printf("Enter cost (rr.kk) \n");
    scanf ("%d.%d",&rubles,&kop);
    if (kop <= 0 || kop > 99 || rubles < 0)
    {
        printf ("Wrong value \n");
        return 1;
    }
    else
    {
        printf("%d rubles , %d kops \n", rubles, kop);
        return 0;
    }
}