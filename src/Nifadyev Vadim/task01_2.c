#include <stdio.h>
#include <math.h>

int main()
{
	int feet, inch, sm;
	printf("Enter your height: (feet'inches) ");
	scanf_s("%d'%d", &feet, &inch);
	if (feet < 0 || inch < 0)
	{
		printf("Input error! Try again \n");
	    return 1;
    }
	if (inch >= 12)
	{
		feet += inch / 12;
		inch = inch % 12;
		printf("Your height in american system is %d'%d \n", feet, inch);
	}
	sm = (feet * 12 + inch) * 2.54;
	if ( feet >= 0 || inch >= 0 || sm >= 0)
        printf("Your height in european system is %d \n", sm);
	return 0;
}