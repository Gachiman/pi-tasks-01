#include <stdio.h>

int main()
{
	int rub; int kop;
	printf("Enter the number (xxx.xxx) ");
	scanf_s("%d.%d", &rub, &kop);
	if (rub < 0 || kop < 0)
	{
		printf("Input error! Try again \n");
	    return 1;
    }
	if (kop >= 100)
	{
		rub += kop / 100;
		kop = kop % 100;
		printf("%d rubles %d kopecks \n", rub, kop);
	}
	else
		printf("%d publes %d kopecks \n ", rub, kop);
	return 0;
}