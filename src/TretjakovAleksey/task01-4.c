#include <stdio.h>
int main()
{
	char arr[256];
	int i, j;
	printf("Enter a line:");
	fgets(arr, 256, stdin);
	j = (strlen(arr) - 1) % 3;
	for (i = 0; i < strlen(arr); ++i)
	{
		printf("%c", arr[i]);
		if ((i + 1) % 3 == j)
			printf(" ");
	}
	return 0;
}